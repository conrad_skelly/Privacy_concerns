# Privacy Concerns
Let's first start off with a simple question: what is Online Privacy?
Online Privacy is your personal data that is stored online.
With the ever growing and ever evolving internet, more and more people use and store data on it, however the same could be said for people or companies trying to steal or exploit that personal info.
A result of this, people's personal data feels less and less safe and rightly so. 
What are some examples that could cause these privacy concerns, well some examples include search engines tracking users, identity theft, computer viruses, scammers/phishers, and etc.
Nowadays there are many ways to protect your personal info from malicious peopleor companies trying to make a profit, like getting a VPN, using strong passwords, reconizing shady links and emails when you see them, and etc.
# what are the government acts that protect your privacy
The government has passed 7 act to protect people privacy online
1. The Federal Trade Commission Act: makes is that companpinies can't show misleading adds
2. The Electronic Communications Privacy Act: Protects certain wire, oral and electroinic communications from unautorized interception, access, use, and disclouare
3. The Computer Fraud & Abuse Act: makes it so people can't access a computer to get information, defraud, obtain anything of value, tramsmit harmful items, or traffic in computer passwords
4. The Children’s Online Privacy Protection Act: requires websites and sevice providers to get parental consent before they collect, use or disclose personal information also requiers sevice providers to post a privacy policy collect only necessary information and create and maintain security measures
5. The CAN-SPAM Act: unsolicited commercial email and prohibits misleading header information and deceptive subject lines. It also requires senders to disclose certain information, include a valid opt-out mechanism, and it creates civil and criminal penalties for violations
6. The Financial Services Modernization Act: regulates the collection, use, and disclosure of personal information collected or held by financial institutions and requires customer notices and a written information security program
7. The Fair and Accurate Credit Transactions Act: requires financial institutions and creditors to maintain written identity theft prevention programs
these are some of the way the federal government protects your internet privacy
# why is internet privacy important 
companys can take a lot of information on you like your name ,phone number ,payment information, your address, your email address, social security numbers, IP address, passwords ect
companys can use this information to create a custom profile on you that will help them target you with ads taylored towards you and if the compony is owned by a chinese compony then they obligated to share the data they collect to the government like what is listed above 
another reson it is important to have internet privacy is if thier is a data breach people wont get your personal data
# what can you do to protect your internet privacy
1. Deleate unused mobile apps and browser extensions: to see your Chrome extenstions you can type chrome://extensions/ into the search bar make sure to deleate any extenstions you do not use
2. Block search engines from tracking you: to deleate your Chrome data go to my activity dashboard and from there you can deleate your youtube web and app data and stop google from collecting that data theres no way to deleat all your data from google so you may want to consider switching to a search engine like DuckDuckGo
3. disable ad and data tracking: the easiest thing you can do to protect your internet privacy is when a app asks you to allow to track your activity across ofther companies say no
